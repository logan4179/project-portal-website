import * as React from 'react';
import ProjectPreview from './components/ProjectPreview/ProjectPreview.component';
import MainSiteContainer from './components/MainSiteContainer.component';

const App = () => {
    return (
    <div>
      <MainSiteContainer></MainSiteContainer>
    </div>
  );
}

export default App;
