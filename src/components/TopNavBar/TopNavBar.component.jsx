import './TopNavBar.styles.scss';

import SignedInUserControl from './SignedInUserControl/SignedInUserControl.component';

const TopNavBar = () =>
{
    const imgFolderPath = "../graphics/";

    return(
        <div id="top-nav-bar-parent-div">
            <div id="top-nav-bar_left-div">
            <img src={imgFolderPath + "SiteLogo_optionA.png"} id="img_site-log-icon" />
            </div>

            <div id="top-nav-bar_right-div">
                <a href="#" class="top-navbar-link ">Sign-in</a>
                <SignedInUserControl />
                <img src={imgFolderPath + "message-indicator.png"} id="img_mssg-icon" />
                <img src={imgFolderPath + "notification-icon-placeholder.jpg"} id="img_notif-icon" />
            </div>
        </div>

    );
}

export default TopNavBar