import './SignedInUserControl.styles.scss';


const SignedInUserControl = () =>
{
    const imgFolderPath = "../graphics/";

    return(
        <div id="div_signed-in-user-control_parent" class="highlighted-hover-container">
            <img src={imgFolderPath + "avatar-placeholder.png"} id="img_user-avatar" />
            <img src={imgFolderPath + "Arrow-web-down.png"} id="img_down-arrow" />
        </div>

    );
}

export default SignedInUserControl