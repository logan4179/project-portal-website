import './NewestPanel.styles.scss';
import NewestProjectPreview from './NewestProjectPreview/NewestProjectPreview.component';

const NewestPanel = () =>
{
    return(
        <div id="div_newest-panel_parent">
            <p id="p_newest-projects">Newest Projects</p>
            <NewestProjectPreview />
            <NewestProjectPreview />
            <NewestProjectPreview />
            <NewestProjectPreview />
            <NewestProjectPreview />
            <NewestProjectPreview />

        </div>

    );
}

export default NewestPanel