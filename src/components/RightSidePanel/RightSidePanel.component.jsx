import './RightSidePanel.styles.scss';
import NewestPanel from './NewestPanel/NewestPanel.component';
import AdPanel
 from './AdPanel/AdPanel.component';
const RightSidePanel = () =>
{

    return(
        <div id="rsp-outer-div">
            <NewestPanel />
            <AdPanel />
        </div>

    );
}

export default RightSidePanel