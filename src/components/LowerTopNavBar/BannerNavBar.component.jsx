import './BannerNavBar.styles.scss';

import VideoGameSearchOptions from '../Banner/ProjectTypeSearchOptions/VideoGameSearchOptions/VideoGameSearchOptions.component';

const BannerNavBar = () =>
{
    const projectTypes = ["Any", "Video Games", "Software/Web", "Music", "Other/Uncategorized"]; //todo: is this okay?
    const sortOptions = ["Hot", "Rating", "Newest"];

    //todo: what is the value used for in a select?
    return(
        <div id="div_BannerNavBar_parent">
            
            <div id="div_universal-search-options">
                <div id="div_project-type" class="div_search-option">
                    <p id="p_proj-type">Project Type: </p>
                    <select name="project-type" class="select_search-option" id="select_project-type">
                        {projectTypes.map(prjType => (
                            <option key={prjType.indexOf} value={prjType}>{prjType}</option>
                        ))}
                    </select>
                </div>

                <div class="div_search-option">
                    <p>Sort By: </p>
                    <select name="sortBy-selection" class="select_search-option" id="select_payStructure-type">
                        {sortOptions.map(srtOptn => (
                            <option key={srtOptn.indexOf} value={srtOptn}>{srtOptn}</option>
                        ))}
                    </select>
                </div>
            </div>

            
            <div id="div_current-search-options">
                <VideoGameSearchOptions />
            </div>
        </div>

    );
}

export default BannerNavBar