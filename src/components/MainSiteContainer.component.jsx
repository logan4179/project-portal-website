import '../components/MainSiteContainer.styles.scss';

import Banner from './Banner/Banner.component.jsx';
import LandingPage from './LandingPage/LandingPage.component.jsx';
import ProjectPreviewList from './ProjectPreview/ProjectPreviewList.component.jsx';
import RightSidePanel from './RightSidePanel/RightSidePanel.component.jsx';
import SiteFooter from './SiteFooter/SiteFooter.component.jsx';

const MainSiteContainerAlt = () =>
{
    const dummyProjectPreviewArray = [
        {
            title:"Metal Gear Solid",
            id:1,
            username:"hKojima99",
            description:"An action-adventure stealth video game developed and published by Konami for the PlayStation in 1998. It was directed, produced, and written by Hideo Kojima, and follows the MSX2 video games Metal Gear and Metal Gear 2: Solid Snake, which Kojima also worked on.[8] It was unveiled at the 1996 Tokyo Game Show and then demonstrated at trade shows including the 1997 Electronic Entertainment Expo; its Japanese release was originally planned for late 1997, before being delayed to 1998.",
            mainGenre:"Action",
            needs: ["Programmer","3d artist"],
            communityRating: 9.5,
            mainImgSrc: "mgs1_main.jpeg",
            platform: "Playstation",
            images: [""],
        },
        {
            title:"Devil May Cry",
            id:2,
            username:"kamiya-san42",
            description:"A 2001 action-adventure game developed and published by Capcom. Released from August to December, originally for the " +
            "PlayStation 2, it is the first installment in the Devil May Cry series. Set in modern times on the fictional Mallet Island, the story " + 
            "centers on Dante, a demon hunter who uses his business to carry out a lifelong vendetta against all demons. He meets a woman named Trish " +
            "who takes him on a journey to defeat the demon lord Mundus, who is responsible for the deaths of Dante's brother and mother. The story is " + 
            "told primarily through a mixture of cutscenes, which use the game engine and several pre-rendered full motion videos. The game is very " + 
            "loosely based on the Italian poem Divine Comedy by the use of allusions, including the game's protagonist Dante (named after Dante Alighieri)" + 
            " and other characters like Trish (Beatrice Portinari) and Vergil (Virgil).",
            mainGenre:"Action",
            needs: ["Musician","Texture artist"],
            communityRating: 9.8,
            mainImgSrc: "dmc1.jpeg",
            platform: "Playstation 2",
            images: [],

        },
        {
            title:"Silent Hill",
            id:3,
            description:"a 1999 survival horror game developed by Team Silent, a group in Konami Computer Entertainment Tokyo and published by Konami. The first installment in the video game series Silent Hill, the game was released from February to July, originally for the PlayStation. Silent Hill uses a third-person view, with real-time rendering of 3D environments. To mitigate limitations of the console hardware, developers used fog and darkness to muddle the graphics. Unlike earlier survival horror games that focused on protagonists with combat training, the player character of Silent Hill is an 'everyman'",
            mainGenre:"ewrer",
            needs: ["asdf","fdsfs"],
            communityRating: 5,
            mainImgSrc: "silent-hill-1.jpg",
            platform: "Playstation",
            images: [],

        },
        {
            title:"[Title Here]",
            id:4,
            description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.zpurus ut. Id consectetur purus ut faucibus pulvinar.",
            mainGenre:"ewrer",
            needs: ["asdf","fdsfs"],
            communityRating: 5,
            mainImgSrc: "gamepad.jpg",
            platform: "Playstation",
            images: [],

        },
        {
            title:"[Title Here]",
            id:5,
            description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.zpurus ut. Id consectetur purus ut faucibus pulvinar.",
            mainGenre:"ewrer",
            needs: ["asdf","fdsfs"],
            communityRating: 5,
            mainImgSrc: "gamepad.jpg",
            platform: "Playstation",
            images: [],

        },
        {
            title:"[Title Here]",
            id:6,
            description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.zpurus ut. Id consectetur purus ut faucibus pulvinar.",
            mainGenre:"ewrer",
            needs: ["asdf","fdsfs"],
            communityRating: 5,
            mainImgSrc: "gamepad.jpg",
            platform: "Playstation",
            images: [],

        },
    ];

    return(
        <div id="div_main-site-container_parent">
            <Banner />
            <div id="div_main-page-content">
                <ProjectPreviewList class="project-preview-list" queriedProjects={dummyProjectPreviewArray}></ProjectPreviewList>
                <RightSidePanel></RightSidePanel> 
            </div>

            <SiteFooter /> 
            <LandingPage project={dummyProjectPreviewArray[0]} />
        </div>

    );
}

export default MainSiteContainerAlt