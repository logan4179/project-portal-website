import './VideoGameSearchOptions.styles.scss';


const VideoGameSearchOptions = () =>
{
    const genreOptions = ["Any", "Action", "Platformer", "VR", "Sim" ]; //todo: is this okay?
    const platformOptions = ["Any", "PC", "Mobile", "Playstation", "Nintendo", "Xbox", "Handheld", "Other" ];
    const helpOptions = ["Any", "Programmer", "3D Artist", "Concept Artist", "Texture Artist", "Animator", "Design/Writing", "Other" ];
    const payStructure = ["Any", "Revshare", "Paid" ];

    return(
        <div id="div_parent">
            <div class="div_search-option">
                <p>Genre: </p>
                <select name="genre-selection" class="select_search-option" id="select_genre-type">
                    {genreOptions.map(gnre => (
                        <option key={gnre.indexOf} value={gnre}>{gnre}</option>
                    ))}
                </select>
            </div>

            <div class="div_search-option">
                <p>Platform: </p>
                <select name="platform-selection" class="select_search-option" id="select_platform-type">
                    {platformOptions.map(pltfrm => (
                        <option key={pltfrm.indexOf} value={pltfrm}>{pltfrm}</option>
                    ))}
                </select>
            </div>

            <div class="div_search-option">
                <p>Needs: </p>
                <select name="needs-selection" class="select_search-option" id="select_needs-type">
                    {helpOptions.map(hlpOptn => (
                        <option key={hlpOptn.indexOf} value={hlpOptn}>{hlpOptn}</option>
                    ))}
                </select>
            </div>

            <div class="div_search-option">
                <p>Pay Structure: </p>
                <select name="payStructure-selection" class="select_search-option" id="select_payStructure-type">
                    {payStructure.map(pyStrctr => (
                        <option key={pyStrctr.indexOf} value={pyStrctr}>{pyStrctr}</option>
                    ))}
                </select>
            </div>
        </div>
    );
}

export default VideoGameSearchOptions