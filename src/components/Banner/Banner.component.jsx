import './Banner.scss';

import TopNavBar from '../TopNavBar/TopNavBar.component';
import BannerNavBar from '../LowerTopNavBar/BannerNavBar.component';

const Banner = () =>
{
    return(
        <div id="banner-parent-div">
            <TopNavBar/>
            <h1 id="site-title">The Project Portal</h1>
            <h3 id="site-subtitle">Dive into anything!</h3>
            <BannerNavBar />
        </div>

    );
}

export default Banner