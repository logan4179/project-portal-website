import './LandingPage.styles.scss';


const LandingPage = (props) =>
{
    const imgFolderPath = "../ProjectImages/";
    console.log(props.project.images.isArray);
    return(
        <div id="div_LandingPage_parent">
            <div id="div_LandingPage_titleBar">
                <h1 id="h1_landing-page-title">{props.project.title}</h1>
                <div id="div_LandingPage_infoBar">
                    <div id="div_LandingPage_username">
                        <p>{props.project.username}</p>

                    </div>
                    <p>{props.project.platform}</p>
                </div>

                
                <div id="div_LandingPage_thumbnails">
                    <img src={imgFolderPath + props.project.mainImgSrc}></img>
                </div>

            </div>
        </div>

    );
}

export default LandingPage