import './ProjectPreviewList.styles.scss';
import ProjectPreview from './ProjectPreview.component';

const ProjectPreviewList = (props) =>
{
    return(
        
        <div id="proj-prev-list_parent-div">
            {props.queriedProjects.map((proj) => {
				return <ProjectPreview project={proj} />;
			})};

        </div>

    );
}

export default ProjectPreviewList