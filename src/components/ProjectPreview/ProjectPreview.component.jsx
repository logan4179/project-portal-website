import './ProjectPreview.styles.scss';

const ProjectPreview = (props) =>
{
    const imgFolderPath = "../ProjectImages/";
    console.log(props.project.communityRating);

    /*const constructNeedsString (origArray) => { //need to iterate over array and construct a comma-separated string...
        return (
            origArray.map((item) => (
            
        )  ));
    };*/

    return(
        <div class="div_project-preview_parent" onClick={"location.href='#';"}>
            <a href="#" class="a_project-preview-div"></a>

            <div class="div_project-preview_highlight highlighted-hover-container">
                <div class="div_project-preview-top-bar">
                    <img src={"../Graphics/heart-icon_placeholder.jpg"} class="img_project-prev-title_community-rating" alt="project"/>
                    <p class="p_community-rating">{props.project.communityRating}</p>
                    <p class="p_project-prev-title p_project-preview-top-bar">{props.project.title}</p>
                    <p class="p_project-preview-top-bar">{props.project.mainGenre}</p>
                    <div class="div_needs">
                        <p class="p_needs p_project-preview-top-bar">{props.project.needs}</p>
                    </div>
                </div>

                <div class="div_project-prev_main-content">
                    <div class="div_project-prev-desc_img-container">
                        <img src={imgFolderPath + props.project.mainImgSrc} class="img_project-prev-desc_thumbnail-img"/>

                    </div>

                    <p class="p_project-prev-description">
                        {props.project.description}
                    </p>
                </div>
            </div>
        </div>

    );
}

export default ProjectPreview